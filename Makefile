### Configuration
IMAGE_BUILD_FROM = registry.gitlab.com/modioab/base-image/fedora-$(FEDORA_ROOT_RELEASE)/python:master
IMAGE_REPO = registry.gitlab.com/modioab/anti
IMAGE_ARCHIVE = image.tar

.DEFAULT_GOAL = all
include build.mk
